<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'objets_virtuels_titre' => 'Objets virtuels',
	'objet_virtuel_titre' => 'Objet virtuel',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
	'erreur_creation_champ_virtuel_dans_tables' => 'Le champ <code>virtuel</code> n’a pas pu être créé dans la ou les tables : @tables@',

	// I
	'info_renvoi_objet' => '<strong>Redirection.</strong> Cet objet renvoie à la page :',

	// L
	'label_activer_objets_virtuels' => 'Quels objets éditoriaux peuvent être virtuels ?',
	'label_activer_objets_virtuels_explication' => 'Ces objets disposeront d’un formulaire pour indiquer une redirection éventuelle',

	// T
	'titre_page_configurer_objets_virtuels' => 'Configuration des objets acceptant des redirections',
	'texte_reference_mais_redirige' => 'Objet référencé, mais redirigé vers une autre URL.',
);
